package com.bogdan.pelikan96;

import java.util.Scanner;

public class AliceAndBob {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please,decide how big Alice's party will be");
        int membersSize = scan.nextInt();
        scan.close();
        int pplWhoKnows = 1;

        boolean[] party = new boolean[membersSize];
        party[1] = true;      // imagine that [0] is Alice and [1] is Bob;
        for (int i = 0; i < party.length; i++) {

            int rng = 1 + (int) (Math.random() * (membersSize - 1));

            if (party[rng]) break;
            party[rng] = true;
            pplWhoKnows += 1;
        }
        System.out.println("Precentage of ppl who Knows: " + (double) pplWhoKnows * 100 / (membersSize-1));
    }
}
